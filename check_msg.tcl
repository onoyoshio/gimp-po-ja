set file [lindex $argv 0]

proc check_msg {msgid msgstr} {
    if {$msgid eq $msgstr} return
    if {$msgid eq "" || $msgstr eq ""} return

    set idx [string first _ $msgid]
    if {$idx != -1} {
        set accel [string range $msgid $idx $idx+1]
        if {$accel ne $msgid || $accel ne $msgstr} {
            if {[regexp -all -nocase "\($accel\)" $msgstr] == 0} {
                puts "  [file tail $::file]:ACCELKEY MISSING: \[$msgid\]=\[$msgstr\]"
            }
        }
    }
    
    if {[regexp ".*\\((_\\w)\\).*" $msgstr m s] == 1} {
        if {[string match -nocase "*$s*" $msgid] == 0} {
                puts "  [file tail $::file]:INVALID ACCELKEY EXISTS: \[$msgid\]=\[$msgstr\]"
        }
    }
}

set fp [open $file]
fconfigure $fp -encoding utf-8

set cur_type -1
set cur_token -1

while {![eof $fp]} {
    set line [gets $fp]
    if {$line eq ""} {
        continue
    }
    if {[string index $line 0] eq "#"} {
        continue
    }
    if {[string range $line 0 6] eq "msgctxt"} {
        continue
    }
    if {[string range $line 0 4] eq "msgid" ||
        [string range $line 0 5] eq "msgstr"} {

        set type  [lindex [split $line " "] 0]
        set token [string trim [string range $line [string length $type]+1 end] " "]
        set token [string trim $token "\""]
        
        if {$type ne $cur_type} {
            if {$cur_type eq "msgid"} {
                set msgid $cur_token
            }
            if {$cur_type eq "msgstr"} {
                set msgstr $cur_token
                check_msg $msgid $msgstr
            }
            set cur_type $type
            set cur_token $token
        }

        continue
    }
    if {[string range $line 0 0] eq "\""} {
        append cur_token [string trim $line "\""]
        continue
    }
}
close $fp
