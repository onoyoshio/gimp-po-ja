INSTALLDIR="/c/Program Files/GIMP 2/share/locale/ja/LC_MESSAGES"

MO_FILES=gimp20.mo \
		 gegl-0.4.mo \
		 gimp20-std-plug-ins.mo \
		 gimp20-script-fu.mo \
		 gimp20-python.mo\
		 gimp20-tips.mo \
		 gimp20-libgimp.mo

all: $(MO_FILES)

build: $(MO_FILES)
	zip -u gimp-2-10-po-ja.zip $(MO_FILES)

install: $(MO_FILES)
	cp $^ $(INSTALLDIR)/

gimp20.mo: gimp.gimp-2-10.ja.po
	msgfmt $^ -o $@

gegl-0.4.mo: gegl.po.master.ja.po
	msgfmt $^ -o $@

gimp20-std-plug-ins.mo: gimp-plug-ins.gimp-2-10.ja.po
	msgfmt $^ -o $@

gimp20-script-fu.mo: gimp-script-fu.gimp-2-10.ja.po
	msgfmt $^ -o $@

gimp20-python.mo: gimp-python.gimp-2-10.ja.po
	msgfmt $^ -o $@

gimp20-tips.mo: gimp-tips.gimp-2-10.ja.po
	msgfmt $^ -o $@

gimp20-libgimp.mo: gimp-libgimp.gimp-2-10.ja.po
	msgfmt $^ -o $@

update-po:
	msgmerge --update gimp.master.ja.po gimp.master.pot
	msgmerge --update gimp.gimp-2-10.ja.po gimp.gimp-2-10.pot
	msgmerge --update gegl.po.master.ja.po gegl.master.pot
	msgmerge --update gimp-plug-ins.master.ja.po gimp-plug-ins.master.pot
	msgmerge --update gimp-plug-ins.gimp-2-10.ja.po gimp-plug-ins.gimp-2-10.pot
	msgmerge --update gimp-script-fu.master.ja.po gimp-script-fu.master.pot
	msgmerge --update gimp-script-fu.gimp-2-10.ja.po gimp-script-fu.gimp-2-10.pot
	msgmerge --update gimp-python.master.ja.po gimp-python.master.pot
	msgmerge --update gimp-python.gimp-2-10.ja.po gimp-python.gimp-2-10.pot
	msgmerge --update gimp-tips.master.ja.po gimp-tips.master.pot
	msgmerge --update gimp-tips.gimp-2-10.ja.po gimp-tips.gimp-2-10.pot
	msgmerge --update gimp-libgimp.master.ja.po gimp-libgimp.master.pot
	msgmerge --update gimp-libgimp.gimp-2-10.ja.po gimp-libgimp.gimp-2-10.pot
	msgmerge --update gimp.po-windows-installer.master.ja.po gimp-windows-installer.master.pot
	msgmerge --update gimp.po-windows-installer.gimp-2-10.ja.po gimp-windows-installer.gimp-2-10.pot

update-pot:
	wget https://l10n.gnome.org/POT/gimp.master/gimp.master.pot
	wget https://l10n.gnome.org/POT/gimp.gimp-2-10/gimp.gimp-2-10.pot
	wget https://l10n.gnome.org/POT/gegl.master/gegl.master.pot
	wget https://l10n.gnome.org/POT/gimp.master/gimp-plug-ins.master.pot
	wget https://l10n.gnome.org/POT/gimp.gimp-2-10/gimp-plug-ins.gimp-2-10.pot
	wget https://l10n.gnome.org/POT/gimp.master/gimp-script-fu.master.pot
	wget https://l10n.gnome.org/POT/gimp.gimp-2-10/gimp-script-fu.gimp-2-10.pot
	wget https://l10n.gnome.org/POT/gimp.master/gimp-python.master.pot
	wget https://l10n.gnome.org/POT/gimp.gimp-2-10/gimp-python.gimp-2-10.pot
	wget https://l10n.gnome.org/POT/gimp.master/gimp-tips.master.pot
	wget https://l10n.gnome.org/POT/gimp.gimp-2-10/gimp-tips.gimp-2-10.pot
	wget https://l10n.gnome.org/POT/gimp.master/gimp-libgimp.master.pot
	wget https://l10n.gnome.org/POT/gimp.gimp-2-10/gimp-libgimp.gimp-2-10.pot
	wget https://l10n.gnome.org/POT/gimp.master/gimp-windows-installer.master.pot
	wget https://l10n.gnome.org/POT/gimp.gimp-2-10/gimp-windows-installer.gimp-2-10.pot

clean:
	rm -fr *.mo
	rm -fr *~
	rm -fr *.pot

test:
	msgfmt --check gimp.master.ja.po
	msgfmt --check gimp.gimp-2-10.ja.po
	msgfmt --check gegl.po.master.ja.po
	msgfmt --check gimp-plug-ins.master.ja.po
	msgfmt --check gimp-plug-ins.gimp-2-10.ja.po
	msgfmt --check gimp-script-fu.master.ja.po
	msgfmt --check gimp-script-fu.gimp-2-10.ja.po
	msgfmt --check gimp-python.master.ja.po
	msgfmt --check gimp-python.gimp-2-10.ja.po
	msgfmt --check gimp-tips.master.ja.po
	msgfmt --check gimp-tips.gimp-2-10.ja.po
	msgfmt --check gimp-libgimp.master.ja.po
	msgfmt --check gimp-libgimp.gimp-2-10.ja.po
	msgfmt --check gimp.po-windows-installer.master.ja.po
	msgfmt --check gimp.po-windows-installer.gimp-2-10.ja.po
	tclsh check_msg.tcl gimp.master.ja.po
	tclsh check_msg.tcl gimp.gimp-2-10.ja.po
	tclsh check_msg.tcl gegl.po.master.ja.po
	tclsh check_msg.tcl gimp-plug-ins.master.ja.po
	tclsh check_msg.tcl gimp-plug-ins.gimp-2-10.ja.po
	tclsh check_msg.tcl gimp-script-fu.master.ja.po
	tclsh check_msg.tcl gimp-script-fu.gimp-2-10.ja.po
	tclsh check_msg.tcl gimp-python.master.ja.po
	tclsh check_msg.tcl gimp-python.gimp-2-10.ja.po
	tclsh check_msg.tcl gimp-tips.master.ja.po
	tclsh check_msg.tcl gimp-tips.gimp-2-10.ja.po
	tclsh check_msg.tcl gimp-libgimp.master.ja.po
	tclsh check_msg.tcl gimp-libgimp.gimp-2-10.ja.po
	tclsh check_msg.tcl gimp.po-windows-installer.master.ja.po
	tclsh check_msg.tcl gimp.po-windows-installer.gimp-2-10.ja.po
